<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_h_t_v_s', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('canHo_ma')->nullable()->comment('Mã căn hộ');
            $table->foreign('canHo_ma')
                ->references('canHo_ma')->on('can_hos')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->string('thanhVien_ma')->nullable()->comment('Mã thành viên');
            $table->foreign('thanhVien_ma')
                ->references('thanhVien_ma')->on('thanh_viens')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->string('thanhVien_loai')->comment('Loại thành viên')->default('2')->comment('Loại thành viên # 1-chủ sở hữu, 2-thành viên');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch-ketnoi-tvs');
    }
};
