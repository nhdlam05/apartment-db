<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_dons', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('tienDien')->nullable();
            $table->bigInteger('tienNuoc')->nullable();
            $table->bigInteger('tienGuixe')->nullable();
            $table->bigInteger('tongTien')->nullable();
            $table->tinyInteger('hoaDon_trangThai')->default('2')->comment('Trạng thái # Trạng thái căn hộ: 1-đã sở hữu, 2-chưa sở hữu');
            $table->string('canHo_ma')->nullable()->comment('Mã căn hộ');
            $table->foreign('canHo_ma')
                ->references('canHo_ma')->on('can_hos')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_dons');
    }
};
