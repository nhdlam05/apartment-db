<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('can_hos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->timestamps();
            // cot
            $table->string('canHo_ma')->comment('Mã căn hộ');
            $table->string('canHo_ten', 191)->nullable()->comment('Tên căn hộ # Tên căn hộ');
            $table->unsignedInteger('canHo_giaBan')->default('0')->comment('Giá bán # Giá bán hiện tại của căn hộ');
            $table->string('canHo_hinh', 200)->nullable()->comment('Hình đại diện # Hình đại diện của căn hộ');
            $table->text('canHo_thongTin')->nullable()->comment('Thông tin # Thông tin về căn hộ');
            $table->tinyInteger('canHo_trangThai')->default('2')->comment('Trạng thái # Trạng thái căn hộ: 1-đã sở hữu, 2-chưa sở hữu');
            // unique
            $table->unique(['canHo_ma']);

        });
        DB::statement("ALTER TABLE `can_hos` comment 'Căn hộ # căn hộ'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('can-ho');
    }
};
