<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thanh_viens', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->timestamps();
            $table->string('thanhVien_ma', 191)->nullable()->comment('Tên thành viên # Tên thành viên');
            $table->string('thanhVien_ten', 191)->nullable()->comment('Tên căn hộ # Tên căn hộ');
            $table->text('thanhVien_CCCD')->nullable()->comment('Thông tin # Thông tin về căn hộ');
            $table->string('thanhVien_gioiTinhh')->nullable()->default('nam')->comment('Giới tính # Giới tính thành viên: 1-Nam, 2-Nữ');
            $table->unique(['thanhVien_ma']);

        });
        DB::statement("ALTER TABLE `thanh_viens` comment 'Thành viên # Thành viên'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thanh_viens');
    }
};
