@extends('layout')

@section('content')
    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row d-flex align-items-center">
                        <div class="col-sm-6">
                            <h2>Quản lý <b>Thành Viên</b></h2>
                        </div>
                            <div class="col-sm-6" style="display: flex;justify-content: flex-end; align-items: center;">
                                <a style="display: flex; align-items: center; margin-right: 5px" href="{{route('thanhvien.create') }}" class="btn btn-success" data-toggle="modal">

                                    <i class="material-icons">&#xE147;</i>
                                    <span style="margin-left: 5px">Thêm thành viên</span>
                                </a>
                            </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>

                        <th>Mã Thành Viên</th>
                        <th>Tên Thành Viên</th>
                        <th>Căn Hộ</th>
                        <th>Loại</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($thanhvien as $tv)
                        <tr>
                            <td>{{ $tv ->thanhVien_ma }}</td>
                            <td>{{ $tv ->thanhVien_ten }}</td>
                            <td>{{ $tv ->canho }}</td>
                            @if ($tv ->loai == 1)
                            <td>Chủ sở hữu</td>
                            @elseif ($tv ->loai == 2)
                            <td>Thành viên</td>
                            @endif
                            <td>
                                <a href="{{route('thanhvien.edit',$tv->id) }}" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                <button ondblclick="{{route('thanhvien.destroy', $tv->id) }}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></button>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Delete Modal HTML -->
@endsection
