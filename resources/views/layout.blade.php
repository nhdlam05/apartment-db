<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">

    <link rel="apple-touch-icon" type="image/png"
          href="https://cpwebassets.codepen.io/assets/favicon/apple-touch-icon-5ae1a0698dcc2402e9712f7d01ed509a57814f994c660df9f7a952f3060705ee.png" />
    <meta name="apple-mobile-web-app-title" content="CodePen">

    <link rel="shortcut icon" type="image/x-icon"
          href="https://cpwebassets.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico" />

    <link rel="mask-icon" type="image/x-icon"
          href="https://cpwebassets.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg"
          color="#111" />


    <title>Quản lý chung cư</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
{{--    <link href="{{ asset('style.css') }}" rel="stylesheet">--}}


    <style>
        body {
            margin-top: 50px;
            background-color: #fff;
            font-family: Arial, sans-serif;
            font-size: 14px;
            letter-spacing: 0.01em;
            color: #39464e;
        }

        .navbar-default {
            background-color: #FFF;
            margin-left: 200px;
        }

        /*main side bar*/
        .msb {
            width: 200px;
            background-color: #F5F7F9;
            position: fixed;
            left: 0;
            top: 0;
            right: auto;
            min-height: 100%;
            overflow-y: auto;
            white-space: nowrap;
            height: 100%;
            z-index: 1;
            border-right: 1px solid #ddd;
        }

        .msb .navbar {
            border: none;
            margin-left: 0;
            background-color: inherit;
        }

        .msb .navbar-header {
            width: 100%;
            border-bottom: 1px solid #e7e7e7;
            margin-bottom: 20px;
            background: #fff;
        }

        .msb .navbar-nav .panel {
            border: 0 none;
            box-shadow: none;
            margin: 0;
            background: inherit;
        }

        .msb .navbar-nav li {
            display: block;
            width: 100%;
        }

        .msb .navbar-nav li a {
            padding: 15px;
            color: #5f5f5f;
        }

        .msb .navbar-nav li a .glyphicon,
        .msb .navbar-nav li a .fa {
            margin-right: 8px;
        }

        .msb .nb {
            padding-top: 5px;
            padding-left: 10px;
            margin-bottom: 30px;
            overflow: hidden;
        }

        ul.nv,
        ul.ns {
            position: relative;
            padding: 0;
            list-style: none;
        }

        .nv {
            /*ns: nav-sub*/
        }

        .nv li {
            display: block;
            position: relative;
        }

        .nv li::before {
            clear: both;
            content: "";
            display: table;
        }

        .nv li a {
            color: #444;
            padding: 10px 25px;
            display: block;
            vertical-align: middle;
        }

        .nv li a .ic {
            font-size: 16px;
            margin-right: 5px;
            font-weight: 300;
            display: inline-block;
        }

        .nv .ns li a {
            padding: 10px 50px;
        }

        /*main content wrapper*/
        .mcw {
            margin-left: 200px;
            position: relative;
            min-height: 100%;
            /*content view*/
        }

        /*globals*/
        a,
        a:focus,
        a:hover {
            text-decoration: none;
        }

        .inbox .container-fluid {
            padding-left: 0;
            padding-right: 0;
        }

        .inbox ul,
        .inbox li {
            margin: 0;
            padding: 0;
        }

        .inbox ul li {
            list-style: none;
        }

        .inbox ul li a {
            display: block;
            padding: 10px 20px;
        }

        .msb,
        .mnb {
            -moz-animation: slidein 300ms forwards;
            -o-animation: slidein 300ms forwards;
            -webkit-animation: slidein 300ms forwards;
            animation: slidein 300ms forwards;
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .mcw {
            -moz-animation: bodyslidein 300ms forwards;
            -o-animation: bodyslidein 300ms forwards;
            -webkit-animation: bodyslidein 300ms forwards;
            animation: bodyslidein 300ms forwards;
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        body.msb-x .mcw,
        body.msb-x .mnb {
            margin-left: 0;
            -moz-animation: bodyslideout 300ms forwards;
            -o-animation: bodyslideout 300ms forwards;
            -webkit-animation: bodyslideout 300ms forwards;
            animation: bodyslideout 300ms forwards;
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        body.msb-x .msb {
            -moz-animation: slideout 300ms forwards;
            -o-animation: slideout 300ms forwards;
            -webkit-animation: slideout 300ms forwards;
            animation: slideout 300ms forwards;
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        /* Slide in animation */
        @-moz-keyframes slidein {
            0% {
                left: -200px;
            }

            100% {
                left: 0;
            }
        }

        @-webkit-keyframes slidein {
            0% {
                left: -200px;
            }

            100% {
                left: 0;
            }
        }

        @keyframes slidein {
            0% {
                left: -200px;
            }

            100% {
                left: 0;
            }
        }

        @-moz-keyframes slideout {
            0% {
                left: 0;
            }

            100% {
                left: -200px;
            }
        }

        @-webkit-keyframes slideout {
            0% {
                left: 0;
            }

            100% {
                left: -200px;
            }
        }

        @keyframes slideout {
            0% {
                left: 0;
            }

            100% {
                left: -200px;
            }
        }

        @-moz-keyframes bodyslidein {
            0% {
                left: 0;
            }

            100% {
                margin-left: 200px;
            }
        }

        @-webkit-keyframes bodyslidein {
            0% {
                left: 0;
            }

            100% {
                left: 0;
            }
        }

        @keyframes bodyslidein {
            0% {
                margin-left: 0;
            }

            100% {
                margin-left: 200px;
            }
        }

        @-moz-keyframes bodyslideout {
            0% {
                margin-left: 200px;
            }

            100% {
                margin-right: 0;
            }
        }

        @-webkit-keyframes bodyslideout {
            0% {
                margin-left: 200px;
            }

            100% {
                margin-left: 0;
            }
        }

        @keyframes bodyslideout {
            0% {
                margin-left: 200px;
            }

            100% {
                margin-left: 0;
            }
        }
    </style>

    <script>
        window.console = window.console || function (t) {};
    </script>



    <script>
        if (document.location.search.match(/type=embed/gi)) {
            window.parent.postMessage("resize", "*");
        }
    </script>


</head>

<body translate="no">
<nav class="mnb navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <i class="ic fa fa-bars"></i>
            </button>
            <div style="padding: 15px 0;">
                <a href="#" id="msbo"><i class="ic fa fa-bars"></i></a>
            </div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">User<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li style="display: flex; justify-content: center">
                            <form method="POST" action="{{ route('logout') }}" class="inline">
                                @csrf

                                <button type="submit" class="btn btn-success">
                                    Đăng xuất
                                </button>
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>

        </div>
    </div>
</nav>
<!--msb: main sidebar-->
<div class="msb" id="msb">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="brand-wrapper">
                <!-- Brand -->
                <div class="brand-name-wrapper">
                    <a class="navbar-brand" href="#">
                        Quản lý chung cư
                    </a>
                </div>

            </div>

        </div>

        <!-- Main Menu -->
        <div class="side-menu-container">
            <ul class="nav navbar-nav">

                <li class="{{ Route::is('canho.*') || Route::is('canho.*')  ? 'active' : '' }}">
                    <a href="{{route('canho.index') }}"><i class="fa fa-dashboard"></i> Căn hộ</a>
                </li>
                <li class="{{ Route::is('thanhvien.*') || Route::is('thanhvien.*')  ? 'active' : '' }}">
                    <a href="{{route('thanhvien.index') }}"><i class="fa fa-puzzle-piece"></i> Thành Viên</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-heart"></i> Hoá đơn</a>
                </li>

                <!-- Dropdown-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>
<!--main content wrapper-->
<div class="mcw" style="height: calc(100vh - 50px); padding: 30px">
    <!--navigation here-->
    <!--main content view-->
    @yield("content")
</div>
<script
    src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-2c7831bb44f98c1391d6a4ffda0e1fd302503391ca806e7fcc7b9b87197aec26.js"></script>

<script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
{{--<script src="{{ asset('script.js') }}"></script>--}}
<script id="rendered-js">
    (function () {
        $('#msbo').on('click', function () {
            $('body').toggleClass('msb-x');
        });
    })();
    //# sourceURL=pen.js
</script>



</body>

</html>
