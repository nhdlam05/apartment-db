@extends("layout")

@section("content")
    <div class="container-xl">
        <div class="table-responsive" style="overflow: hidden">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row" style="display: flex; align-items: center">
                        <div class="col-sm-6">
                            <h2>Thêm <b>Hóa đơn</b></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <form action="{{route('hoadon.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Căn hộ cần thêm</label>
                            <input type="text" name="canHo_ma" class="form-control" placeholder="Nhập mã căn hộ">
                        </div>
                        <div class="form-group">
                            <label>Loại</label>
                            <select name="hoaDon_trangThai" id="" class="form-control">
                                <option value="1">Đã thanh toán</option>
                                <option value="2">Thanh toán</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mã hóa đơn</label>
                            <input type="text" name="thanhVien_ma" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Số tiền điện</label>
                            <input type="text" name="tienDien" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Số tiền nước</label>
                            <input type="text" name="tienNuoc" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Số tiền gửi xe</label>
                            <input type="text" name="tienGuixe" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-default" data-dismiss="modal" href="{{route('hoadon.index') }}">Huỷ bỏ</a>
                        <input type="submit" class="btn btn-success" value="Thêm">
                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
