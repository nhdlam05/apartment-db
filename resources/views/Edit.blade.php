@extends("layout")

@section("content")
    <div class="container-xl">
        <div class="table-responsive" style="overflow: hidden">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row" style="display: flex; align-items: center">
                        <div class="col-sm-6">
                            <h2>Chỉnh sửa <b>Thành viên: {{ $thanhvien->thanhVien_ma }}</b></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <form action="{{route('thanhvien.update', $thanhvien->id)}}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>Họ và tên chủ sở hữu</strong>
                            <input type="text" name="thanhVien_ten" value="123" class="form-control" placeholder="Nhập họ và tên">
                        </div>
                        <div class="form-group">
                            <strong>Loại</strong>
                            <select name="thanhVien_loai" id="" class="form-select">
                                <option value="1">Chủ sở hữu</option>
                                <option value="2">Thành viên</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-default" data-dismiss="modal" href="{{route('canho.index') }}">Huỷ bỏ</a>
                        <input type="submit" class="btn btn-success" value="Chỉnh sửa">
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection
