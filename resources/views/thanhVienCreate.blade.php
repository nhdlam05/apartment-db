@extends("layout")

@section("content")
    <div class="container-xl">
        <div class="table-responsive" style="overflow: hidden">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row" style="display: flex; align-items: center">
                        <div class="col-sm-6">
                            <h2>Thêm <b>Thành viên</b></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <form action="{{route('thanhvien.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" name="thanhVien_ten" class="form-control" placeholder="Nhập Gía căn hộ">
                        </div>
                        <div class="form-group">
                            <label>Loại</label>
                            <select name="thanhVien_loai" id="" class="form-control">
                                <option value="1">Chủ sở hữu</option>
                                <option value="2">Thành viên</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mã thành viên</label>
                            <input type="text" name="thanhVien_ma" class="form-control" placeholder="Nhập mã thành viên">
                        </div>
                        <div class="form-group">
                            <label>Căn hộ</label>
                            <select name="canHo_ma" id="" class="form-control">
                                <option value="">Chọn căn hộ</option>
                                @foreach($danhSachCanHo as $ch)
                                    <option value="{{ $ch->canHo_ma }}">{{ $ch->canHo_ma }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-default" data-dismiss="modal" href="{{route('canho.index') }}">Huỷ bỏ</a>
                        <input type="submit" class="btn btn-success" value="Thêm">
                    </div>
                </form>

            </div>

        </div>

    </div>
@endsection
