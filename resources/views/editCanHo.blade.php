@extends("layout")

@section("content")
    <div class="container-xl">
        <div class="table-responsive" style="overflow: hidden">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row" style="display: flex; align-items: center">
                        <div class="col-sm-6">
                            <h2>Chỉnh sửa <b>Căn hộ: {{ $canho->canHo_ma }}</b></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <form action="{{route('canho.update', $canho->id)}}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Chủ sở hữu</label>
                            <select name="thanhVien_ma" id="" class="form-control">
                                <option value="">Không có chủ sở hữu</option>
                                @foreach($danhSachThanhVien as $tv)
                                    <option value="{{ $tv->thanhVien_ma }}">{{ $tv->thanhVien_ten }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Gía</label>
                            <input type="number" name="canHo_giaBan" class="form-control" placeholder="Nhập Gía căn hộ">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-default" data-dismiss="modal" href="{{route('canho.index') }}">Huỷ bỏ</a>
                        <input type="submit" class="btn btn-success" value="Chỉnh sửa">
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection
