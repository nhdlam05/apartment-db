@extends('layout')

@section('content')
    <div class="container-xl">
        <div class="table-responsive" style="overflow: hidden">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row" style="display: flex; align-items: center">
                        <div class="col-sm-6">
                            <h2>Quản lý <b>Căn hộ </b></h2>
                        </div>
                        <div class="col-sm-6" style="display: flex;justify-content: flex-end; align-items: center;">
                            <a style="display: flex; align-items: center; margin-right: 5px" href="{{route('canho.create') }}" class="btn btn-success" data-toggle="modal">

                                <i class="material-icons">&#xE147;</i>
                                <span style="margin-left: 5px">Thêm căn hộ</span>
                            </a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Mã căn hộ</th>
                        <th>Tên chủ sở hữu</th>
                        <th>Tình trạng</th>
                        <th>Giá</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($canHo as $ch)
                            <tr>
                                <td>
                                    {{ $ch->canHo_ma }}
                                </td>
                                <td>
                                    {{ $ch->chuSoHuu_ten }}
                                </td>
                                <td>
                                    {{ ($ch->canHo_trangThai == '1') ? 'Đã sở hữu' : 'Chưa sở hữu' }}
                                </td>
                                <td>
                                    {{ $ch->canHo_giaBan }}
                                </td>
                                <td>

                                <td>
                                    <a href="{{route('canho.edit',$ch->id) }}" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                    <button ondblclick="{{route('canho.destroy', $ch->id) }}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
