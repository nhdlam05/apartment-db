<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoaDon extends Model
{
    use HasFactory;
    protected $fillable = [
        // unique
        "tienDien",
        "tienNuoc",
        "tienGuixe",
        "tongTien",
        "canHo_ma",
        "hoaDon_trangThai"
        // lien ket -> thanh_vien ["thanhVien_ma"]
    ];
}
