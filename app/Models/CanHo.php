<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanHo extends Model
{
    use HasFactory;
    protected $fillable = [
        // unique
        "canHo_ma",
        "canHo_ten",
        "canHo_giaBan",
        "canHo_hinh",
        "canHo_thongTin",
        "canHo_trangThai",
        // lien ket -> thanh_vien ["thanhVien_ma"]
    ];
}
