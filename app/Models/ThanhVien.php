<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThanhVien extends Model
{
    use HasFactory;
    protected $fillable = [
        // unique
        "thanhVien_ten",
        "thanhVien_CCCD",
        "thanhVien_taoMoi",
        "thanhVien_capNhat",
        "thanhVien_gioiTinhh",
        "thanhVien_Loai",
        // lien ket -> thanh_vien ["thanhVien_ma"]
    ];
}
