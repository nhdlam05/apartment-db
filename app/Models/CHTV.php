<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CHTV extends Model
{
    use HasFactory;
    protected $fillable = [
        "canHo_ma",
        "thanhVien_ma",
        "thanhVien_loai"
    ];
}
