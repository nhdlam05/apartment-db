<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CanHo;
use App\Models\CHTV;
use App\Models\ThanhVien;
use App\Models\HoaDon;
class HoaDonController extends Controller
{
    public function index() {
        $hoadon = Hoadon::all();

        return view('index')->with('canHo', $hoadon);
    }
    public function create() {
        return view('createHoaDon');
    }
    public function store(Request $request) {
        $hd = new HoaDon();
        $hd->canHo_ma=$request->canHo_ma;
        $hd->tienDien = $request->tienDien;
        $hd->tienNuoc = $request->tienNuoc;
        $hd->tienGuixe = $request->tienGuixe;
        $hd->hoaDon_trangThai = $request->hoaDon_trangThai;
        $hd->save();
        return redirect()-> route("hoadon.index");
    }
    public function edit(CanHo $canho) {
        $ds_thanhVien = ThanhVien::all();
        return view('editCanHo',compact('canho'))->with('danhSachThanhVien', $ds_thanhVien);;
    }
    public function update(Request $request, CanHo $canho) {
        $chtv = CHTV::where(
            [
                'canHo_ma',$canho->canHo_ma,
                'thanhVien_loai', '1'
            ]
        )->first();
        $chtv->thanhVien_ma = $request->thanhVien_ma;
        $chtv->thanhVien_loai = "1";
        $chtv->save();
        $canho ->update($request->all());
        return redirect()-> route('canho.index')-> with('thongbao', 'Cập nhật thành công! ');
    }

    public function destroy(CanHo $canho) {
        $canho ->delete();
        return redirect()-> route('canho.index')->with("thongbao","Xóa thành công! ");
    }
}
