<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CanHo;
use App\Models\CHTV;
use App\Models\ThanhVien;
use Termwind\Components\Dd;

class ThanhVienController extends Controller
{
    public function index() {
        $thanhvien = ThanhVien::paginate(5);
        foreach ($thanhvien as $tv) {
            $chtv = CHTV::where('thanhVien_ma',$tv->thanhVien_ma)->first();
            if (isset($chtv)) {
                $tv->loai = $chtv->thanhVien_loai;
                $tv->canho = $chtv->canHo_ma;
            }
        }
        return view('indexThanhVien',compact("thanhvien")) -> with("i", (request() -> input("page",1)-1) * 5);
    }
    public function create() {
        $ds_canHo = CanHo::where("canHo_trangThai",2)->get();
        //  dd($ds_canHo);
        return view('thanhVienCreate') ->with('danhSachCanHo', $ds_canHo);

    }
    public function store(Request $request) {
        $tv = new ThanhVien();
        $tv->thanhVien_ten=$request->thanhVien_ten;
        $tv->thanhVien_ma=$request->thanhVien_ma;
        $tv->save();
        $chtv = new CHTV();
        $chtv->canHo_ma = $request->canHo_ma;
        $chtv->thanhVien_ma = $request->thanhVien_ma;
        $chtv->thanhVien_loai = $request->thanhVien_loai;
        $chtv->save();
        $ds_canHo = CanHo::where('canHo_ma',$request->canHo_ma)->get();
        foreach($ds_canHo as $ch) {
            $ch->canHo_trangThai = "1";
            $ch->save();
        }
        return redirect()-> route("thanhvien.index");
    }
    public function edit(ThanhVien $thanhvien) {
        return view('edit',compact('thanhvien'));
    }
    public function update(Request $request, ThanhVien $thanhvien) {
        $ds_chtv = CHTV::where('thanhVien_ma',$thanhvien->thanhVien_ma)->get();
        foreach($ds_chtv as $chtv) {
            $chtv->thanhVien_loai = $request->thanhVien_loai;
            $chtv->save();
        }
        $thanhvien->update($request->all());
        return redirect()-> route('thanhvien.index')-> with('thongbao', 'Cập nhật thành công! ');
    }
    public function destroy(ThanhVien $thanhvien) {
        $thanhvien ->delete();
        return redirect()-> route('thanhvien.index')->with("thongbao","Xóa thành công! ");
    }
}
