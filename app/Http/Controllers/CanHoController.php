<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CanHo;
use App\Models\CHTV;
use App\Models\ThanhVien;
class CanHoController extends Controller
{
    public function index() {
        $canho = CanHo::all();

        foreach ($canho as $ch) {
            $chtv = CHTV::where('canHo_ma',$ch->canHo_ma)->first();
            if (isset($chtv)) {
                $CSH = ThanhVien::where('thanhVien_ma', $chtv->thanhVien_ma)->first();
                $ch->chuSoHuu_ten = $CSH->thanhVien_ten;
            }
        }
        return view('index')->with('canHo', $canho);
    }
    public function create() {
        $ds_thanhVien = ThanhVien::all();
        return view('create') ->with('danhSachThanhVien', $ds_thanhVien);
    }
    public function store(Request $request) {
        $ch = new CanHo();
        $ch->canHo_ma=$request->canHo_ma;
        $ch->canHo_giaBan=$request->canHo_giaBan;
        if (isset($request->thanhVien_ma)) {
            $ch->canHo_trangThai="1";
        } else {
            $ch->canHo_trangThai="2";
        }
        $ch->save();

        if (isset($request->thanhVien_ma)) {

            $chtv = new CHTV();
            $chtv->canHo_ma = $request->canHo_ma;
            $chtv->thanhVien_ma = $request->thanhVien_ma;
            $chtv->thanhVien_loai = "1";
            $chtv->save();
        }



        return redirect()-> route("canho.index");
    }
    public function edit(CanHo $canho) {
        $ds_thanhVien = ThanhVien::all();
        return view('editCanHo',compact('canho'))->with('danhSachThanhVien', $ds_thanhVien);;
    }
    public function update(Request $request, CanHo $canho) {
        $chtv = CHTV::where(
            [
                'canHo_ma',$canho->canHo_ma,
                'thanhVien_loai', '1'
            ]
        )->first();
        $chtv->thanhVien_ma = $request->thanhVien_ma;
        $chtv->thanhVien_loai = "1";
        $chtv->save();
        $canho ->update($request->all());
        return redirect()-> route('canho.index')-> with('thongbao', 'Cập nhật thành công! ');
    }

    public function destroy(CanHo $canho) {
        $canho ->delete();
        return redirect()-> route('canho.index')->with("thongbao","Xóa thành công! ");
    }
}
