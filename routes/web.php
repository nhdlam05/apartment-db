<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CanHoController;
use App\Http\Controllers\ThanhVienController;
use App\Http\Controllers\HoaDonController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    return redirect('/canho');
});

Route::resource("/canho",CanHoController::class)->middleware('auth');
Route::resource("/thanhvien",ThanhVienController::class)->middleware('auth');


//Route::middleware([
//    'auth:sanctum',
//    config('jetstream.auth_session'),
//    'verified'
//])->group(function () {
//    Route::get('/canho', function () {
//        return view('index');
//    })->name('index');
//});
